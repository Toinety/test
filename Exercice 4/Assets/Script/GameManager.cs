﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class GameManager {

	private static GameManager instance = null;
	private static readonly object padlock = new object ();
	public int niveauEnCours = 2;

	GameManager()
	{
	}

	public static GameManager Instance
	{
		get 
		{
			lock (padlock) 
			{
				if (instance == null) 
				{
					instance = new GameManager ();
				}
				return instance;
			}
		}
	}
}
