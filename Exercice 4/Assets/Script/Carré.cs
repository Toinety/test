﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carré : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void bougezCarrés()
	{
		Vector3 pos = transform.position;
		pos.x += Time.deltaTime;
		transform.position = pos;
	}

	void OnMouseDown()
	{
		Debug.Log ("clic");
		transform.parent.parent.BroadcastMessage ("bougezCarrés");
		transform.parent.parent.BroadcastMessage ("RougisCercle");
	}
	IEnumerator RougisCarré()
	{
		SpriteRenderer monSprite = GetComponent<SpriteRenderer> ();
		for(int i = 0; i <= 99; i++) 
		{
			if (monSprite.color == Color.red) 
			{
				monSprite.color = Color.white;
			} 
			else 
			{
				monSprite.color = Color.red;
			}
			yield return null;
		}
	}
}
