﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class LevelManager : MonoBehaviour {

	private static LevelManager instance = null;
	private static readonly object padlock = new object ();
	public GameObject prefCarre;
	public GameObject Ronds;

	LevelManager()
	{
	}

	public static LevelManager Instance
	{
		get 
		{
			lock (padlock) 
			{
				if (instance == null) 
				{
					instance = new LevelManager ();
				}
				return instance;
			}
		}
	}

	void Start()
	{
		GameObject mesCarré = Instantiate(prefCarre);
		GameObject mesRond = Instantiate(Ronds);
		GameObject Cont = new GameObject();
		mesRond.transform.parent = Cont.transform;
		mesCarré.transform.parent = Cont.transform;
	}
}
