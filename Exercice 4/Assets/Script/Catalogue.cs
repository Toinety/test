﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Catalogue : ScriptableObject
{
    public List<ObjetInventaire> Inventaire;
    public Catalogue()
    {
		
    }
    // Use this for initialization
    void OnEnable()
    {
		Inventaire = new List<ObjetInventaire>();
		Debug.Log( "constructeur en cours...");
		string nom;
		float poid;
		float prix;
		for (int i = 0; i <= 150; i++)
		{
			poid = Random.Range(0f, 10000f);
			Debug.Log(poid);
			prix = Random.Range(0f, 10000f);
			Debug.Log(prix);
			System.Guid monGuid = System.Guid.NewGuid();
			nom = monGuid.ToString();
			Debug.Log(nom);
			ObjetInventaire monOgj = new ObjetInventaire(nom, poid, prix);
			Inventaire.Add(monOgj);
		}
		Debug.Log("Constructeur Fini!");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
